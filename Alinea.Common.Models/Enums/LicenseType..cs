﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Alinea.Common.Models.Enums
{
	public enum LicenseType
	{
		Normal = 1,
		Showcase,
		Floating,
		Fixed
	}
}
