﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Alinea.Common.Models.Dtos
{
	public class LicenseDto
	{
		public Guid AccountId { get; set; }
		public Guid Id { get; set; }
		public string Name { get; set; }
		public int AccessType { get; set; }
		public string AccessTypeName { get; set; }
		public Guid? CategoryId { get; set; }
		public int LicenseType { get; set; }
		public string LicenseTypeName { get; set; }
		public DateTime CreatedAt { get; set; }
		public DateTime? ModifiedAt { get; set; }
		public DateTime? StartDate { get; set; }
		public DateTime? EndDate { get; set; }
		public int AvailableSeats { get; set; }
		public bool IsDeleted { get; set; }
		public bool IsPaused { get; set; }
		public string CategoryName { get; set; }
		public int Buffer { get; set; }
		public string Grade { get; set; }
		public string UserGroupName { get; set; }
		public int UserGroupType { get; set; }
		public string UserGroupTypeName { get; set; }
		public string UserGroupIdentifier { get; set; }
		public IList<Guid> UserIds { get; set; }
		public IList<string> ProductIsbns { get; set; }
		public IList<Guid> AllowableUserRoleIds { get; set; }
		public IList<string> AllowableUserRoleNames { get; set; }
		public IList<Guid> PrivilegedUserRoleIds { get; set; }
		public Guid UserGroupId { get; set; }
		public IList<Guid> InstitutionsInMunicipality { get; set; }
		public DateTime? AgreementStartDate { get; set; }
		public DateTime? AgreementEndDate { get; set; }
		public IList<Guid> SeatUserGroupIds { get; set; }
		public int? LicenseCategoryTypeId { get; set; }
		public string LicenseCategoryTypeName { get; set; }
	}
}
