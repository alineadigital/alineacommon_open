﻿using System;

namespace Alinea.Common.Utils
{
	public static class SystemTime
	{
		public const string Format = "yyyy-MM-dd";

		private static readonly Func<DateTime> DefaultLogic = () => DateTime.UtcNow;
		private static Func<DateTime> _current = DefaultLogic;

		public static DateTime Current => _current();

		public static void _replaceCurrentLogic(Func<DateTime> newLogic)
		{
			_current = newLogic;
		}

		public static void _revertToDefaultLogic()
		{
			_current = DefaultLogic;
		}
	}
}
